const readline = require('readline');

// set cordinates
var moveByOne = 0;

var robo = {
    roboFace: '',
    get faceName() {
        return this.roboFace;
    },
    set faceName(name) {
        this.roboFace = name;
    },
    roboX: '',
    get roboXValue() {
        return this.roboX;
    },
    set roboXValue(name) {
        this.roboX = name;
    },
    roboY: '',
    get roboYValue() {
        return this.roboY;
    },
    set roboYValue(name) {
        this.roboY = name;
    }
}

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log('Lets PLACE the ROBOT x,y,face ... ');
rl.question('PLACE x,y,face : ', (answer) => {
    var individualValue = answer.split(',');
    var x = individualValue[0];
    var y = individualValue[1];
    var face = individualValue[2];

    // pass all these values to setName function, which set dynamic setter and getter for all the values
    setName(x, y, face);

    //checkConditions(x, y, face);
    //rl.close();
});


function setName(x, y, face) {
    // console.log(x + " " + y + " " + face);
    robo.roboXValue = x;
    robo.roboYValue = y;
    robo.faceName = face;
    //console.log('       x : ' + robo.roboXValue + "      y : " + robo.roboYValue + '        face : ' + robo.faceName);
    checkConditions(robo.roboXValue, robo.roboYValue, robo.faceName);
}

// To ensure that the robot should not move beyond the boundries
// set limitations
var checkConditions = function(x, y, face) {
    // check : x and y should not be less than 0 and greater than 4. that is 5 * 5 units
    if (x < 0 || x > 4 || y < 0 || y > 4) {
        console.log('Please select x and y values in betweeen  0 to 4 range... try again ');
    } else {
        //console.log('good to go...');
        // check face
        if (face.toUpperCase() == 'NORTH' || face.toUpperCase() == 'SOUTH' || face.toUpperCase() == 'EAST' || face.toUpperCase() == 'WEST') {
            //console.log('Right face...' + x, y, face);
            // take other inputs
            // if some types move then go to move function or someone types left then function or same for right and report.
            console.log('Select MOVE, LEFT, RIGHT, REPORT, EXIT : ');
            rl.on('line', (input) => {
                //console.log(`Received: ${input}`);
                if (input.toLowerCase() == 'move') {
                    move()
                } else if (input.toLowerCase() == 'left') {
                    left(robo.roboXValue, robo.roboYValue, robo.faceName);
                } else if (input.toLowerCase() == 'right') {
                    right(robo.roboXValue, robo.roboYValue, robo.faceName);
                } else if (input.toLowerCase() == 'report') {
                    //finalReport(y, x, face);
                    finalReport(robo.roboYValue, robo.roboXValue, robo.faceName);
                } else if (input.toLowerCase() == 'exit') {
                    exit()
                } else {
                    console.log('Something went wrong try again...');
                    exit()
                }

            });
        } else {
            rl.close();
            console.log('wrong face... try again');
        }
    }
}

var move = function() {
    //console.log('In side move function');
    // if y=0, and x = 0
    // then move north or east
    //console.log('-------------------- ' + robo.faceValue);

    // set value
    // robo.faceValue = 'UPPPPP'
    //console.log('==============' + robo.faceValue);


    moveByOne++;
    if (robo.roboFace.toLowerCase() == 'north') {
        if (robo.roboXValue >= 0 && robo.roboYValue < 4) {
            robo.roboYValue++;
            //console.log('Direction... is ' + face + " and x value is " + x);
            //robo.faceValue = x;
            robo.roboXValue = robo.roboXValue;
            robo.roboYValue = robo.roboYValue;
        } else {
            reportCantMove(robo.roboXValue, robo.roboYValue, robo.faceName);
        }
    } else if (robo.roboFace.toLowerCase() == 'east') {
        //console.log('inside ' + x + " " + y + " " + face);
        if (robo.roboXValue < 4 && robo.roboYValue < 4) {
            robo.roboXValue++;
            // console.log('Direction... is ' + face + " and y value is " + y);
            robo.roboXValue = robo.roboXValue;
            robo.roboYValue = robo.roboYValue;
        } else {
            reportCantMove(robo.roboXValue, robo.roboYValue, robo.faceName);
        }
    } else if (robo.roboFace.toLowerCase() == 'south') {
        //console.log('inside ' + x + " " + y + " " + face);
        if (robo.roboXValue >= 0 && robo.roboYValue >= 1) {
            robo.roboYValue--;
            // console.log('Direction... is ' + face + " and x value is " + x);
            robo.roboXValue = robo.roboXValue;
            robo.roboYValue = robo.roboYValue;
        } else {
            reportCantMove(robo.roboXValue, robo.roboYValue, robo.faceName);
        }
    } else if (robo.roboFace.toLowerCase() == 'west') {
        //console.log('inside west' + x + " " + y + " " + face);
        if (robo.roboXValue >= 1 && robo.roboYValue >= 1) {
            robo.roboXValue--;
            // console.log('Direction... is ' + face + " and y value is " + y);
            robo.roboXValue = robo.roboXValue;
            robo.roboYValue = robo.roboYValue;
        } else {
            reportCantMove(robo.roboXValue, robo.roboYValue, robo.faceName);
        }
    }
}

var left = function(x, y, face) {
    //console.log('In Left : ' + x, y, face);
    if (face.toLowerCase() == 'north') {
        //face = 'west';
        //console.log('Left turn face     X:' + x + "      y:" + y + "    face:" + face);
        // finalReport(x, y, update_face);
        robo.faceName = 'west'
        robo.roboXValue = x;
        robo.roboYValue = y;
        //console.log('Update left values: X:' + robo.roboXValue + "      y:" + robo.roboYValue + "    face:" + robo.faceName);
    } else if (face.toLowerCase() == 'west') {
        //robo.faceValue = 'south';
        //console.log('Left turn face     X:' + x + "      y:" + y + "    face:" + face);
        robo.faceName = 'south'
        robo.roboXValue = x;
        robo.roboYValue = y;
        //console.log('Update left values: X:' + robo.roboXValue + "      y:" + robo.roboYValue + "    face:" + robo.faceName);
    } else if (face.toLowerCase() == 'south') {
        //console.log('Left turn face     X:' + x + "      y:" + y + "    face:" + face);
        robo.faceName = 'east';
        robo.roboXValue = x;
        robo.roboYValue = y;
        //console.log('Update left values: X:' + robo.roboXValue + "      y:" + robo.roboYValue + "    face:" + robo.faceName);
    } else if (face.toLowerCase() == 'east') {
        //console.log('Left turn face     X:' + x + "      y:" + y + "    face:" + face);
        robo.faceName = 'north';
        robo.roboXValue = x;
        robo.roboYValue = y;
        //console.log('Update left values: X:' + robo.roboXValue + "      y:" + robo.roboYValue + "    face:" + robo.faceName);
    }
}

var right = function(x, y, face) {
    // console.log('In Right : ' + x, y, face);
    if (face.toLowerCase() == 'north') {
        robo.faceName = 'east';

        robo.roboXValue = x;
        robo.roboYValue = y;
    } else if (face.toLowerCase() == 'east') {
        robo.faceName = 'south';

        robo.roboXValue = x;
        robo.roboYValue = y;
    } else if (face.toLowerCase() == 'south') {
        robo.faceName = 'west';
        robo.roboXValue = x;
        robo.roboYValue = y;
    } else if (face.toLowerCase() == 'west') {
        robo.faceName = 'north';
        robo.roboXValue = x;
        robo.roboYValue = y;
    }
}


var reportCantMove = function(x, y, face) {
    console.log(`Final Report : You faced at [ ${x} , ${y} , ${face.toUpperCase()} ]. You can not move further. `);
    exit();
}

var finalReport = function(y, x, f) {
    console.log(`Final Report :[ ${x} , ${y} , ${f} ]`);
    exit();
}

var exit = function() {
    rl.close();
    console.log('Thanks');
}