# A Robot game

> The application is a simulation of a robot moving on a square table top, of dimensions 5 x 5 units. 
> It roams on the square from 0 to 4 coordinates.

# Setup
1. npm install
1. Run node robot.js

# Instructions To Play
* Enter x,y,face values 
* Select by typing one of these - MOVE, LEFT, RIGHT, REPORT, EXIT 
* Hit Report to get the final position of the robot

**NOTE: The robot just roams in between 5*5 units. If it is moved beyond that it shows the error and generates the final report.**

### Examples: (TRY THIS INSTRUCTION EXACLTY TO GET AN IDEA)

*INPUT*

1. 2,2,north
1. move
1. move
1. report

*OUTPUT*

* Final Report :[ 2 , 4 , north ]

*INPUT*

1. 2,1,west
1. move
1. move
1. right
1. move
1. report

*OUTPUT*

* Final Report :[ 0 , 2 , north ]

> If user tries to move beyond the bounderies, it generates the report with the error.

*INPUT*

1. 0,4,south
1. move
1. move
1. move
1. move
1. move

*OUTPUT*

* Final Report : You faced at [ 0 , 0 , SOUTH ]. You can not move further.

**ENJOY**
