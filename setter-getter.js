var robo = {
    firstName: '',
    get fullName() {
        return this.firstName;
    },
    set fullName(name) {
        this.firstName = name;
    }
}

robo.fullName = 'Jack';
console.log(robo.firstName); // Jack